<?php

function slugify($text)
{
    $text = preg_replace('~[^\pL\d]+~u', '-', $text);
    $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);
    $text = preg_replace('~[^-\w]+~', '', $text);
    $text = trim($text, '-');
    $text = preg_replace('~-+~', '-', $text);
    $text = strtolower($text);

    if (empty($text)) return 'n-a';

    return $text;
}

$dir_name = slugify($_POST['name']);

exec("mkdir " . $dir_name); // créer un répertoire pour le nouveau site
exec("mkdir " . $dir_name . "/plugins");
exec("mkdir " . $dir_name . "/plugins/auto");

/**
 * Télécharger spip_loader.php
 */
exec("wget https://www.spip.net/spip-dev/INSTALL/spip_loader.php");
exec("mv spip_loader.php " . $dir_name . "/spip_loader.php");

header('Location: ' . $dir_name . '/spip_loader.php');

/**
 * Téléchargement des plugins
 */

//Mini-calendrier
exec("wget https://files.spip.org/spip-zone/spip-contrib-extensions/calendrier_mini-82518-v2.4.1.zip"); // télécharger le plugin
exec("unzip calendrier_mini-82518-v2.4.1.zip"); // dézipper le plugin
exec("rm calendrier_mini-82518-v2.4.1.zip"); // supprimer l'archive
exec("mv calendrier_mini " . $dir_name . "/plugins/auto/"); // déplacer le dossier dans le répertoire des plugins

//Agenda
exec("wget https://files.spip.org/spip-zone/spip-contrib-extensions/agenda-ef595-v3.39.4.zip"); // télécharger le plugin
exec("unzip agenda-ef595-v3.39.4.zip"); // dézipper le plugin
exec("rm agenda-ef595-v3.39.4.zip"); // supprimer l'archive
exec("mv agenda " . $dir_name . "/plugins/auto/"); // déplacer le dossier dans le répertoire des plugins

//Facteur
exec("wget https://files.spip.org/spip-zone/spip-contrib-extensions/facteur-2c4f9-v3.7.2.zip"); // télécharger le plugin
exec("unzip facteur-2c4f9-v3.7.2.zip"); // dézipper le plugin
exec("rm facteur-2c4f9-v3.7.2.zip"); // supprimer l'archive
exec("mv facteur " . $dir_name . "/plugins/auto/"); // déplacer le dossier dans le répertoire des plugins

//En travaux
exec("wget https://files.spip.org/spip-zone/spip-contrib-extensions/en_travaux-90326-v3.2.7.zip"); // télécharger le plugin
exec("unzip en_travaux-90326-v3.2.7.zip"); // dézipper le plugin
exec("rm en_travaux-90326-v3.2.7.zip"); // supprimer l'archive
exec("mv en_travaux " . $dir_name . "/plugins/auto/"); // déplacer le dossier dans le répertoire des plugins

//NoSPAM
exec("wget https://files.spip.org/spip-zone/spip-contrib-extensions/nospam-f85e8-v2.1.6.zip"); // télécharger le plugin
exec("unzip nospam-f85e8-v2.1.6.zip"); // dézipper le plugin
exec("rm nospam-f85e8-v2.1.6.zip"); // supprimer l'archive
exec("mv nospam " . $dir_name . "/plugins/auto/"); // déplacer le dossier dans le répertoire des plugins

//MailSubscribers
exec("wget https://files.spip.org/spip-zone/spip-contrib-extensions/mailsubscribers-90987-v2.14.4.zip"); // télécharger le plugin
exec("unzip mailsubscribers-90987-v2.14.4.zip"); // dézipper le plugin
exec("rm mailsubscribers-90987-v2.14.4.zip"); // supprimer l'archive
exec("mv mailsubscribers " . $dir_name . "/plugins/auto/"); // déplacer le dossier dans le répertoire des plugins

//MailShot
exec("wget https://files.spip.org/spip-zone/spip-contrib-extensions/mailshot-74bac-v1.28.0.zip"); // télécharger le plugin
exec("unzip mailshot-74bac-v1.28.0.zip"); // dézipper le plugin
exec("rm mailshot-74bac-v1.28.0.zip"); // supprimer l'archive
exec("mv mailshot " . $dir_name . "/plugins/auto/"); // déplacer le dossier dans le répertoire des plugins

//Formulaire de contact avancé
exec("wget https://files.spip.org/spip-zone/spip-contrib-extensions/contact-8cf91-v0.16.6.zip"); // télécharger le plugin
exec("unzip contact-8cf91-v0.16.6.zip"); // dézipper le plugin
exec("rm contact-8cf91-v0.16.6.zip"); // supprimer l'archive
exec("mv contact " . $dir_name . "/plugins/auto/"); // déplacer le dossier dans le répertoire des plugins

//SPIPr-dist
exec("wget https://files.spip.org/spip-zone/spip-contrib-squelettes/spipr-dist-d0123-v2.2.6.zip"); // télécharger le plugin
exec("unzip spipr-dist-d0123-v2.2.6.zip"); // dézipper le plugin
exec("rm spipr-dist-d0123-v2.2.6.zip"); // supprimer l'archive
exec("mv spipr-dist " . $dir_name . "/plugins/auto/"); // déplacer le dossier dans le répertoire des plugins

//NoiZetier
if ($_POST['noizetier']) {
    exec("wget https://files.spip.org/spip-zone/spip-contrib-extensions/noizetier-25667-v1.1.5.zip"); // télécharger le plugin
    exec("unzip noizetier-25667-v1.1.5.zip"); // dézipper le plugin
    exec("rm noizetier-25667-v1.1.5.zip"); // supprimer l'archive
    exec("mv noizetier " . $dir_name . "/plugins/auto/"); // déplacer le dossier dans le répertoire des plugins
}

//Zen-Garden
if ($_POST['zen-garden']) {
    exec("wget https://files.spip.org/spip-zone/spip-contrib-extensions/zen-garden-7728b-v2.9.1.zip"); // télécharger le plugin
    exec("unzip zen-garden-7728b-v2.9.1.zip"); // dézipper le plugin
    exec("rm zen-garden-7728b-v2.9.1.zip"); // supprimer l'archive
    exec("mv zen-garden " . $dir_name . "/plugins/auto/"); // déplacer le dossier dans le répertoire des plugins
}

//Pages
if ($_POST['pages']) {
    exec("wget https://files.spip.org/spip-zone/spip-contrib-extensions/pages-70f6d-v1.5.2.zip"); // télécharger le plugin
    exec("unzip pages-70f6d-v1.5.2.zip"); // dézipper le plugin
    exec("rm pages-70f6d-v1.5.2.zip"); // supprimer l'archive
    exec("mv pages " . $dir_name . "/plugins/auto/"); // déplacer le dossier dans le répertoire des plugins
}

//oEmbed
if ($_POST['oembed']) {
    exec("wget https://files.spip.org/spip-zone/spip-contrib-extensions/oembed-b9b21-v2.3.3.zip"); // télécharger le plugin
    exec("unzip oembed-b9b21-v2.3.3.zip"); // dézipper le plugin
    exec("rm oembed-b9b21-v2.3.3.zip"); // supprimer l'archive
    exec("mv oembed " . $dir_name . "/plugins/auto/"); // déplacer le dossier dans le répertoire des plugins
}

//SPIP Bonux
if ($_POST['spip-bonux']) {
    exec("wget https://files.spip.org/spip-zone/spip-contrib-extensions/spip-bonux-d8bab-v3.5.5.zip"); // télécharger le plugin
    exec("unzip spip-bonux-d8bab-v3.5.5.zip"); // dézipper le plugin
    exec("rm spip-bonux-d8bab-v3.5.5.zip"); // supprimer l'archive
    exec("mv spip-bonux " . $dir_name . "/plugins/auto/"); // déplacer le dossier dans le répertoire des plugins
}

//Comments
if ($_POST['comments']) {
    exec("wget https://files.spip.org/spip-zone/spip-contrib-extensions/comments-5d6cc-v2.1.17.zip"); // télécharger le plugin
    exec("unzip comments-5d6cc-v2.1.17.zip"); // dézipper le plugin
    exec("rm comments-5d6cc-v2.1.17.zip"); // supprimer l'archive
    exec("mv comments " . $dir_name . "/plugins/auto/"); // déplacer le dossier dans le répertoire des plugins
}

//Z-core
if ($_POST['z-core']) {
    exec("wget https://files.spip.org/spip-zone/spip-contrib-extensions/z-core-e85bd-v2.8.7.zip"); // télécharger le plugin
    exec("unzip z-core-e85bd-v2.8.7.zip"); // dézipper le plugin
    exec("rm z-core-e85bd-v2.8.7.zip"); // supprimer l'archive
    exec("mv z-core " . $dir_name . "/plugins/auto/"); // déplacer le dossier dans le répertoire des plugins
}

//Adminer
if ($_POST['adminer']) {
    exec("wget https://files.spip.org/spip-zone/spip-contrib-extensions/adminer-770f3-v4.7.8.0.zip"); // télécharger le plugin
    exec("unzip adminer-770f3-v4.7.8.0.zip"); // dézipper le plugin
    exec("rm adminer-770f3-v4.7.8.0.zip"); // supprimer l'archive
    exec("mv adminer " . $dir_name . "/plugins/auto/"); // déplacer le dossier dans le répertoire des plugins
}
